import logging
import re

from glob import glob
from os import path

FILENAME_REGEX = re.compile(r"""(\w+) # Corpus name
                                (\d+\w?) # Transcript ID
                                _
                                (\w\w) # Author initials
                             """,
                            flags=re.X)

def process_pairs(callback, corrections_dir, transcript_dir):
    """ Finds corresponding transcript/correction pairs and calls
    callback(correction_file, transcript_file_ on each.
    """
    for correctfn in sorted(glob(path.join(corrections_dir, '*.csv'))):
        match = FILENAME_REGEX.search(path.basename(correctfn))
        if not match:
            continue
        corpus, basename, author = match.groups()
        if not re.search('^\d\d', basename):
            basename = '0' + basename
        transcript = path.join(transcript_dir,
                               basename.lower() + '.longtr.cex')
        yield callback(correctfn, transcript)

HERE = path.dirname(path.abspath(__file__))

def project_file(file_path):
    return path.join(HERE, file_path)

def get_logger(name, level='INFO'):
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
#    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    logger = logging.getLogger(name)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    if level:
        logger.setLevel(logging.__dict__[level])
    return logger
