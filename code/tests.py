import unittest

from core import Transcript
from utils import project_file

class TranscriptTestCase(unittest.TestCase):

    def test_file_construction(self):
        transcript = Transcript.from_file(
            project_file('fixtures/testfile.longtr.cex'))
        utterances = [
            '{0} - {1} / {2}'.format(u.speaker_id, u.main_tier, u.mor_tier)
            for u in transcript.utterances]

        self.assertEqual(
            utterances,
            ['INV - good morning this is Mother and Child . / adj|good n|morning pro:dem|this v:cop|be&3S n:prop|Mother coord|and n:prop|Child .',
             'MOT - okay . / co|okay .',
             'CHI - &doh +... / None',  # these two are missing mor tiers
             'MOT - xxx . / None',
             'CHI - mommy go . / n|mommy v|go .',
             'MOT - okay . / None'])

        self.assertEqual(transcript.headers,
                         """@UTF8
@Begin
@Languages:	eng
@Participants:	CHI Target_Child, INV Investigator, MOT Mother
@ID:	eng|valian|CHI|2;1.3|female|normal||Target_Child|||
@ID:	eng|valian|INV|||||Investigator|||
@ID:	eng|valian|MOT|||||Mother|||
@Location:	Parent's home
@Date:	04-MAR-1986
@Comment:	Meeting 1""".split('\n'))

        self.assertEqual(transcript.footers, ['@End'])

if __name__ == '__main__':
    unittest.main()
