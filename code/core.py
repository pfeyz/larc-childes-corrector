import csv
import re

from utils import get_logger, process_pairs

logger = get_logger(__name__)

def strip_speaker(s):
    return s.lstrip('*').rstrip(':')

def all_blank(line):
    return all(map(lambda x: x.isspace() or not x, line))

class Utterance(object):
    def __init__(self, uid, speaker_id, main_tier, mor_tier, source):
        self.uid = int(uid)
        self.speaker_id = speaker_id
        self.main_tier = main_tier
        self.mor_tier = mor_tier
        self.source = source

class Transcript(object):
    def __init__(self, headers, utterances, footers, source):
        self.headers = headers
        self.utterances = utterances
        self.footers = footers
        self.source = source

    @classmethod
    def from_file(cls, filename_or_iterable):
        try:  # fileobj
            lines = filename_or_iterable.readlines()
            source = ''
        except AttributeError:  # filename str
            with open(filename_or_iterable) as fh:
                lines = fh.readlines()
            source = filename_or_iterable
        headers = cls.get_headers(lines)
        utterances = [Utterance(uid, speaker, main, mor, source)
                           for uid, (speaker, main, mor) in
                           enumerate(cls.get_tiers(lines))]
        footers = cls.get_footers(lines)
        return Transcript(list(headers), utterances, list(footers), source)

    @staticmethod
    def get_tiers(lines):
        speaker = None
        main, mor = None, None  # tiers
        for line in lines:
            logger.debug('parsing line {0}'.format(line.__repr__()))
            line = re.sub(r'(\s+)', r'\1', line)
            line = line.strip()
            if line.startswith('*'):
                if speaker:  # yield previous line
                    yield strip_speaker(speaker) , main, mor
                speaker, main =  line.split('\t')
                mor = None
            elif line.startswith('%mor:'):
                _, mor =  line.rstrip().split('\t')
        yield strip_speaker(speaker), main, mor


    @staticmethod
    def get_headers(lines):
        for line in lines:
            if line.startswith('@'):
                yield line.strip()
            else:
                break

    @staticmethod
    def get_footers(lines):
        footers = []
        for line in lines[::-1]:
            if line.startswith('@'):
                footers.append(line.strip())
            else:
                break
        return footers[::-1]

class DocumentCorrector(object):
    sniffer = csv.Sniffer()

    def __init__(self, targets=None, source=None):
        if targets is None:
            targets = {}
        self.targets = targets
        self.source = source

    @classmethod
    def from_file(cls, correction_csv_filename):
        document = csv.reader(open(correction_csv_filename))
        start_line = 0
        if cls.sniffer.has_header(correction_csv_filename):
            document.next()
            start_line = 1

        targets = {}
        for line_num, line in enumerate(document):
            line_num += 1 + start_line
            if all_blank(line):
                logger.warning('blank line in {0}:{1}'.format(
                        correction_csv_filename, line_num))
                continue
            logger.debug('creating correction from {0}'.format(line))
            error_msg = 'non-blank trailing columns in correction {0}:{1}'.format(
                correction_csv_filename, line_num)
            try:
                uid, speaker_id, main, mor, error, replacement, notes = line[:7]
                if not all_blank(line[7:]):
                    logger.warning('{0}:{1}'.format(error_msg, line[7:]))
            except ValueError:
                uid, speaker_id, main, mor, error, replacement = line[:6]
                notes = ''
                if not all_blank(line[6:]):
                    logger.warning('{0}:{1}'.format(error_msg, line[6:]))
            uid = int(uid)
            observed_utterance = Utterance(uid, speaker_id, main, mor, '')
            correction = UtteranceCorrector(observed_utterance, error,
                                            replacement, notes, line_num,
                                            correction_csv_filename)
            try:
                targets[uid].append(correction)
            except KeyError:
                targets[uid] = [correction]

        return cls(targets, correction_csv_filename)
        # # skip blank lines
        # if all(map(lambda x: x == '', line)):
        #     continue

    def correct_transcript(self, transcript, outstream):
        outstream.writelines('\n'.join(transcript.headers))
        # for header in transcript.headers:
        #     yield header
        for utterance in transcript.utterances:
            if utterance.uid not in self.targets:
                outstream.write(utterance.format() + '\n')
                continue
            for correction in self.targets[utterance.uid]:
                # TODO: targeted utterances with multiple corrections will yield
                #       multiple lines - this is wrong.
                if not utterance.mor_tier:
                    replaced = yield 'mor missing', utterance, correction
                elif not correction.error:
                    replaced = yield 'mor missing', utterance, correction
                else:
                    matches = re.findall(r'\b{0}\b'.format(
                            re.escape(correction.error)),
                            utterance.mor_tier)
                    if not matches:
                        replaced = yield 'target missing', utterance, correction
                    elif len(matches) == 1:
                        replaced = yield 'normal', utterance, correction
                    elif len(matches) > 1:
                        replaced = yield 'ambiguous', utterance, correction
                outstream.write(replaced.format() + '\n')
                utterance = replaced
        outstream.writelines('\n'.join(transcript.footers))

class UtteranceCorrector(object):

    def __init__(self, observed_utterance, error, replacement, notes,
                 line_num, source):
        self.observed_utterance = observed_utterance
        self.uid = int(observed_utterance.uid)
        self.error = error
        self.replacement = replacement
        self.notes = notes

        self.source = source
        self.line_num = line_num

def wellformed(correction_filename, transcript_filename):
    transcript = Transcript.from_file(transcript_filename)
    for attr in ['headers', 'utterances', 'footers']:
        assert len(transcript.__getattribute__(attr)) > 0, \
            '{0} emtpy in {1}'

    corrector = DocumentCorrector.from_file(correction_filename)
    for correction_list in corrector.targets.values():
        for correction in correction_list:
            for attr in ['error', 'replacement']:
                error = '{0} blank in {1}, line {2}'.format(attr.upper(),
                                                            corrector.source,
                                                            correction.line_num)
                if attr == 'replacement' and correction.notes:
                    logger.warning('{0}\n\t"{1}"'.format(error, correction.notes))
                    continue
                assert bool(correction.__getattribute__(attr)), error



if __name__ == '__main__':
    from sys import argv
    list(process_pairs(wellformed, *argv[1:]))
